use std::{str::FromStr, string::ParseError, io::Read, fmt};

use reqwest::blocking::Client;

pub struct Info {
    pub started: String,
    pub project: String,
    pub branch: String,
    pub commit: String,
    pub config_ref: String,
    pub pungi_ref: String,
    pub osbuild_ref: String,
    pub custom_images_ref: String,
    pub pipeline_as_code_ref: String,
    pub pipeline_url: String,
    pub pipeline_id: String
}

impl Info {
    pub fn new(started: &str, project: &str, branch: &str,
        commit: &str, config_ref: &str, pungi_ref: &str, osbuild_ref: &str,
        custom_images_ref: &str, pipeline_as_code_ref: &str, 
        pipeline_url: &str, pipeline_id: &str) -> Self {
        Self {
            started: started.to_owned(),
            project: project.to_owned(),
            branch: branch.to_owned(),
            commit: commit.to_owned(),
            config_ref: config_ref.to_owned(),
            pungi_ref: pungi_ref.to_owned(),
            osbuild_ref: osbuild_ref.to_owned(),
            custom_images_ref: custom_images_ref.to_owned(),
            pipeline_as_code_ref: pipeline_as_code_ref.to_owned(),
            pipeline_url: pipeline_url.to_owned(),
            pipeline_id: pipeline_id.to_owned()
        }
    }

    pub fn from_reader<R: Read>(mut reader: R) -> Self {
        let mut buffer = Vec::new();

        loop {
            let n = reader.read_to_end(&mut buffer).unwrap();

            if n == 0 {
                break;
            }
        }

        let s = String::from_utf8_lossy(&buffer);

        Self::from_str(&s).unwrap()
    }

    pub fn from_url(url: &str) -> Self {
        let response = Client::new().get(url).send().unwrap();
        if !response.status().is_success() {
            panic!("Error loading remote info file.");
        }

        Info::from_reader(response)
    }
}

impl FromStr for Info {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut info = Info::new("", "", "", "", "", "", "", "", "", "", "");
        let lines: Vec<&str>  = s.split('\n').collect();

        for line in lines {
            // skip if line is empty
            if line.trim().len()  == 0 {
                continue;
            }

            let parts: Vec<&str> = line.split(':').collect(); 

            if parts.len() < 2 {
                panic!("Failed to parse line: \"{line}\"");
            }

            let key: &str = parts[0];
            let value: String = parts[1..parts.len()].join(":");

            match key {
                "Started" => { info.started = value.trim().to_owned(); },
                "Project" => { info.project = value.trim().to_owned(); },
                "Branch" => { info.branch = value.trim().to_owned(); },
                "Commit" => { info.commit = value.trim().to_owned(); },
                "Config REF" => { info.config_ref = value.trim().to_owned(); },
                "Pungi REF" => { info.pungi_ref = value.trim().to_owned(); },
                "OSBUILD REF" => { info.osbuild_ref = value.trim().to_owned(); },
                "Custom Images REF" => { info.custom_images_ref = value.trim().to_owned(); },
                "Pipelines-as-code REF" => { info.pipeline_as_code_ref = value.trim().to_owned(); },
                "Pipeline_URL" => { info.pipeline_url = value.trim().to_owned(); },
                "Pipeline_ID" => {info.pipeline_id = value.trim().to_owned(); },
                &_ => panic!("Invalid key: {key}")
            };
        }

        Ok(info)
    }
}

impl fmt::Display for Info {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {   
        write!(
            f, 
            concat!(
                "Started: {}\n",
                "Project: {}\n",
                "Branch: {}\n",
                "Commit: {}\n",
                "Config REF: {}\n",
                "Pungi REF: {}\n",
                "OSBUILD REF: {}\n",
                "Custom Images REF: {}\n",
                "Pipelines-as-code REF: {}\n",
                "Pipeline_URL: {}\n",
                "Pipeline_ID: {}"
            ),
            self.started,
            self.project,
            self.branch,
            self.commit,
            self.config_ref,
            self.pungi_ref,
            self.osbuild_ref,
            self.custom_images_ref,
            self.pipeline_as_code_ref,
            self.pipeline_url,
            self.pipeline_id
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SAMPLE_INFO: &str = r#"Started: 2024-03-01T02:01:41Z
Project: redhat/edge/ci-cd/pipe-x/pipelines-as-code
Branch: main
Commit: dfb158bea534f026b0710fb7fbbaa0b082d7e2ee
Config REF: main
Pungi REF: main
OSBUILD REF: main
Custom Images REF: main
Pipelines-as-code REF: 
Pipeline_URL: https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code/-/pipelines/1196521568
Pipeline_ID: 1196521568"#;

    #[test]
    fn test_from_str_ok() {
        let info = Info::from_str(SAMPLE_INFO).unwrap();

        assert_eq!(info.started, "2024-03-01T02:01:41Z");
        assert_eq!(info.project, "redhat/edge/ci-cd/pipe-x/pipelines-as-code");
        assert_eq!(info.branch, "main");
        assert_eq!(info.commit, "dfb158bea534f026b0710fb7fbbaa0b082d7e2ee");
        assert_eq!(info.config_ref, "main");
        assert_eq!(info.pungi_ref, "main");
        assert_eq!(info.osbuild_ref, "main");
        assert_eq!(info.custom_images_ref, "main");
        assert_eq!(info.pipeline_as_code_ref, "");
        assert_eq!(info.pipeline_url, "https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code/-/pipelines/1196521568");
        assert_eq!(info.pipeline_id, "1196521568");
    }
    
    #[test]
    #[should_panic]
    fn test_from_str_line_err() {
        Info::from_str("Foo bar").unwrap();
    }

    #[test]
    #[should_panic]
    fn test_from_str_key_err() {
        Info::from_str("Foo: b").unwrap();
    }

    #[test]
    fn from_reader_ok() {
        let r = std::io::Cursor::new(SAMPLE_INFO);
        let info = Info::from_reader(r);

        assert_eq!(info.started, "2024-03-01T02:01:41Z");
        assert_eq!(info.project, "redhat/edge/ci-cd/pipe-x/pipelines-as-code");
        assert_eq!(info.branch, "main");
        assert_eq!(info.commit, "dfb158bea534f026b0710fb7fbbaa0b082d7e2ee");
        assert_eq!(info.config_ref, "main");
        assert_eq!(info.pungi_ref, "main");
        assert_eq!(info.osbuild_ref, "main");
        assert_eq!(info.custom_images_ref, "main");
        assert_eq!(info.pipeline_as_code_ref, "");
        assert_eq!(info.pipeline_url, "https://gitlab.com/redhat/edge/ci-cd/pipe-x/pipelines-as-code/-/pipelines/1196521568");
        assert_eq!(info.pipeline_id, "1196521568");
    }
}
