pub struct Image {
    pub target: String,
    pub distro: String,
    pub image_type: String,
    pub os_type: String,
    pub arch: String,
    pub pipeline_id: String,
    pub commit_hash: String
}

impl Image {
    pub fn new(target: &str, distro: &str,
        image_type: &str, os_type: &str, arch: &str,
        pipeline_id: &str, commit_hash: &str) -> Self {
        Self {
            target: target.to_owned(),
            distro: distro.to_owned(),
            image_type: image_type.to_owned(),
            os_type: os_type.to_owned(),
            arch: arch.to_owned(),
            pipeline_id: pipeline_id.to_owned(),
            commit_hash: commit_hash.to_owned()
        }
    }

    pub fn as_filename(&self) -> String {
        let ext = match self.target.as_str() {
            "qemu" => "qcow2.xz",
            _ => "raw.xz"
        };
        format!("auto-osbuild-{}-{}-{}-{}-{}-{}.{}.{}",
            self.target,
            self.distro,
            self.image_type,
            self.os_type,
            self.arch,
            self.pipeline_id,
            &self.commit_hash[0..8],
            ext)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_as_filename_qemu_ok() {
        let expected = "auto-osbuild-qemu-autosd9-developer-ostree-x86_64-123.dfb158be.qcow2.xz";
        let actual = Image::new("qemu", "autosd9", "developer", "ostree", "x86_64", "123", "dfb158bea534f026b0710fb7fbbaa0b082d7e2ee");

        assert_eq!(expected, actual.as_filename());
    }

    #[test]
    fn test_as_filename_img_ok() {
        let expected = "auto-osbuild-rpi4-autosd9-developer-ostree-aarch64-123.dfb158be.raw.xz";
        let actual = Image::new("rpi4", "autosd9", "developer", "ostree", "aarch64", "123", "dfb158bea534f026b0710fb7fbbaa0b082d7e2ee");

        assert_eq!(expected, actual.as_filename());
    }
}
