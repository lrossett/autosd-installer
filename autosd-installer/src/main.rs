use std::fs::File;
use std::io::prelude::*;

use clap::{Parser, Subcommand};
use crate::repo::Info;
use log::{info, error, debug, LevelFilter};

mod repo;
mod image;

#[derive(Parser)]
struct Cli {
    #[arg(
        short = 'v',
        long = "verbose",
        help = "enable verbose output",
        action = clap::ArgAction::SetTrue
    )]
    verbose: bool,

    #[command(subcommand)]
    command: Option<Commands>
}

#[derive(Subcommand)]
enum Commands {
    // shows program version
    #[command(about = "shows program version")]
    Version {},

    #[command(about = "Downloads and shows the current autosd build info")]
    Info {},

    #[command(about = "downloads an autosd image")]
    Install {
        #[arg(long = "target", default_value="qemu")]
        target: String,
        #[arg(long = "distro", default_value="autosd9")]
        distro: String,
        #[arg(long = "image-type", default_value="developer")]
        image_type: String,
        #[arg(long = "os-type", default_value="regular")]
        os_type: String,
        #[arg(long = "arch", default_value="x86_64")]
        arch: String,
        #[arg(long = "dest", default_value=".")]
        dest: String,
        #[arg(long = "download-only", action = clap::ArgAction::SetTrue)]
        download_only: bool
    }
}

fn main() {
    let base_url = "https://autosd.sig.centos.org/AutoSD-9/nightly";
    let mut is_verbose = false;
    let cli = Cli::parse();
    let mut log_level: LevelFilter = LevelFilter::Info;

    if let Some(true) = Some(cli.verbose) {
        is_verbose = true;
        log_level = LevelFilter::Debug;
    }

    env_logger::Builder::from_default_env()
        .filter_level(log_level)
        .init();

    match &cli.command {
        Some(Commands::Version{}) => {
            if is_verbose {
                println!("autosd-installer-cli v0.0.1");    
            } else {
                println!("v0.0.1");
            }
        },
        Some(Commands::Info{}) => {
            let url = format!("{base_url}/info.txt");

            debug!("Retrieving info from {}...", url);
            
            println!("{}", Info::from_url(&url));
        },
        Some(Commands::Install{target, distro, image_type, os_type, arch, dest, download_only}) => {
            let manifest = Info::from_url(&format!("{base_url}/info.txt"));
            let img = image::Image::new(target, distro, image_type, os_type, arch, &manifest.pipeline_id, &manifest.commit);
            let filename = img.as_filename();
            let url = format!("{base_url}/sample-images/{filename}");

            let response = reqwest::blocking::get(url).expect("failed to get data from url");
            if !response.status().is_success() {
                panic!("Error loading remote info file.");
            }

            let path = format!("{dest}/{filename}");
            let content = response.bytes().expect("failed to read bytes from response");
            let mut file = File::create(path).expect("failed to crrate file");
            file.write_all(&content).expect("failed to write file");

            if *download_only {
                return;
            }
        },
        None => {
            panic!("Unreachable");
        }
    }
}
